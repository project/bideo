<?php

namespace Drupal\bideo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure batch video settings for this site.
 */
class BideoSettingsForm extends ConfigFormBase {
  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'bideo.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bideo_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['batch_video'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Batch Video'),
      '#description' => $this->t('Provide video embed link that has to be played when batch process is running. You can provide multiple values seperated by a new line.'),
      '#default_value' => implode(PHP_EOL, $config->get('batch_video')),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('batch_video', explode(PHP_EOL, $form_state->getValue('batch_video')))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
